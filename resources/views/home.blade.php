@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-md-3"><h5>Latest Articles</h5></div>
                        <div class="col-md-7">
                            <form class="form-inline" method="GET" action="{{ route('home') }}">
                                <label class="sr-only" for="search">Name</label>
                                <input type="text" name="q" class="form-control mb-2 mr-sm-2" id="search" placeholder="Search...">

                                <label class="sr-only" for="category">Username</label>
                                <select name="category" id="category" class="form-control mb-2 mr-sm-2">
                                    <option value="">-- CATEGORY --</option>
                                    @foreach ($categories as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-primary mb-2">Filter</button>
                            </form>
                        </div>
                        <div class="col-md-2">
                            @can('add articles')
                                <a href="{{ route('articles.create') }}" class="btn btn-success mb-2 float-right">New Article</a>
                            @endcan
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @forelse ($articles as $article)
                        <div class="row ">
                            <div class="col-md-3">
                                <img src="{{ $article->image }}" class="card-img" alt="...">
                            </div>
                            <div class="col-md-9">
                                <h5 class="card-title">
                                    <a href="{{ route('articles.show', $article) }}">{{ str_limit($article->title, 50) }}</a>
                                    @if ($article->isDraft())
                                        <span class="badge badge-warning float-right" data-toggle="tooltip" data-placement="top" title="Visible to admins only">
                                            <i class="fas fa-info-circle"></i>
                                            Draft
                                        </span>
                                    @endif
                                </h5>
                                <p class="card-text">{{ str_limit($article->content, 250) }}</p>
                                <p class="card-text">
                                    <small class="text-muted">
                                        Created by {{ $article->owner->name }} {{ $article->created_at->diffForHumans() }} |
                                        {{ $article->comments_count . str_plural(' Comment', $article->comments_count) }}
                                    </small>
                                </p>
                            </div>
                        </div>
                        <hr class="mb-4">
                    @empty
                        Nothing to show at the moment...
                    @endforelse
                </div>
                <div class="card-footer text-center">
                    <div style="display: inline-block">
                        {{ $articles->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
