<div class="form-group row">
    <label for="title" class="col-md-2 col-form-label text-md-right">Title</label>

    <div class="col-md-9">
        {{ Form::text('title', null, ['class' => 'form-control ' . ($errors->has('title') ? 'is-invalid' : '')]) }}

        @error('title')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="category_id" class="col-md-2 col-form-label text-md-right">Category</label>

    <div class="col-md-9">
        {{ Form::select('category_id', $categories, null, ['class' => 'form-control ' . ($errors->has('category_id') ? 'is-invalid' : '')]) }}

        @error('category_id')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="content" class="col-md-2 col-form-label text-md-right">Content</label>

    <div class="col-md-9">
        {{ Form::textarea('content', null, ['id' => 'content', 'class' => 'form-control ' . ($errors->has('content') ? 'is-invalid' : '')]) }}

        @error('content')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="image" class="col-md-2 col-form-label text-md-right">Image</label>

    <div class="col-md-9">
        {{ Form::file('image') }}

        @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <div class="col-md-6 offset-md-2">
        <div class="form-check">
            {{ Form::checkbox('draft', null, isset($article) ? $article->isDraft() : false, ['class' => 'form-check-input']) }}

            <label class="form-check-label" for="draft">
                Draft
            </label>
        </div>
    </div>
</div>

