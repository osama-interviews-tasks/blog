@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    @can('edit articles')
                        <div class="float-left"><a href="{{ route('articles.edit', $article) }}">Edit</a></div>
                    @endcan
                    <div class="float-right"><a href="{{ route('home') }}">Home</a></div>
                </div>

                <div class="card-body">
                    <h5>{{ $article->title }}</h5>
                    <p class="card-text">{{ $article->content }}</p>
                </div>
                @if ($article->isPublished())
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            {{ Form::open(['route' => ['comments.store', $article], 'method' => 'POST', 'class' => 'form-inline']) }}
                                <div class="form-group col-md-10">
                                    <label class="sr-only" for="search">Name</label>
                                    {{ Form::textarea('content', null, ['rows' => 2, 'style' => 'width: 100%', 'class' => 'form-control']) }}
                                </div>
                                <div class="form-group col-md-2">
                                    <button type="submit" class="btn btn-primary mb-2">Comment</button>
                                </div>
                            {{ Form::close() }}
                        </li>
                        @forelse ($article->comments as $comment)
                            <li class="list-group-item">
                                <h5>
                                    <strong>{{ $comment->owner->name }}</strong>
                                    <small>commented {{ $comment->created_at->diffForHumans() }}</small>
                                </h5>
                                <p>{{ $comment->content }}</p>
                            </li>
                        @empty
                            <li class="list-group-item">Be the first to comment...</li>
                        @endforelse
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
