<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use Tests\TestCase;
use App\Enums\ArticleStatus;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user with admin role.
     *
     * @var \App\User
     */
    protected $admin;

    /**
     * A user with no roles.
     *
     * @var \App\User
     */
    protected $visitor;

    /**
     * The setUp method.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
        $this->admin = factory(\App\User::class)->create()->assignRole('admin');
        $this->visitor = factory(\App\User::class)->create();
    }

    /**
     * Testing that admins can create articles.
     *
     * @return void
     */
    public function testAdminsCanCreateArticles()
    {
        $this->actingAs($this->admin);

        $response = $this->get('/articles/create');
        $response->assertStatus(200);

        $response = $this->post('/articles', [
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'image' => UploadedFile::fake()->image('fake.png'),
            'category_id' => Category::first()->id,
        ]);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /**
     * Testing that visitors cannot create articles.
     *
     * @return void
     */
    public function testVisitorsCantCreateArticles()
    {
        $this->actingAs($this->visitor);

        $response = $this->get('/articles/create');
        $response->assertStatus(403);

        $response = $this->post('/articles', [
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'image' => UploadedFile::fake()->image('fake.png'),
            'category_id' => Category::first()->id,
        ]);

        $response->assertForbidden();
    }

    /**
     * Testing that admins can view all articles.
     *
     * @return void
     */
    public function testAdminsCanViewAllArticles()
    {
        $this->actingAs($this->admin);

        $this->admin->articles()->createMany([
            [
                'title' => 'Title',
                'content' => 'Content',
                'status' => ArticleStatus::PUBLISHED,
                'category_id' => Category::first()->id,
            ],
            [
                'title' => 'Title',
                'content' => 'Content',
                'status' => ArticleStatus::DRAFT,
                'category_id' => Category::first()->id,
            ],
        ]);

        $response = $this->get('/home');

        $response->assertSee('Draft');
    }

    /**
     * Testing that visitors can view all published articles.
     *
     * @return void
     */
    public function testVisitorsCanViewAllPublishedArticles()
    {
        $this->actingAs($this->visitor);

        $this->admin->articles()->createMany([
            [
                'title' => 'Title',
                'content' => 'Content',
                'status' => ArticleStatus::PUBLISHED,
                'category_id' => Category::first()->id,
            ],
            [
                'title' => 'Title',
                'content' => 'Content',
                'status' => ArticleStatus::DRAFT,
                'category_id' => Category::first()->id,
            ],
        ]);

        $response = $this->get('/home');

        $response->assertDontSee('Draft');
    }

    /**
     * Testing that admins can view any article.
     *
     * @return void
     */
    public function testAdminsCanViewAnyArticle()
    {
        $this->actingAs($this->admin);

        $published = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $published));
        $response->assertSuccessful();

        $draft = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $draft));
        $response->assertSuccessful();
    }

    /**
     * Testing that visitors can view published articles only.
     *
     * @return void
     */
    public function testVisitorsCanViewPublishedArticles()
    {
        $this->actingAs($this->visitor);

        $published = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $published));
        $response->assertSuccessful();

        $draft = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $draft));
        $response->assertNotFound();
    }

    /**
     * Testing that an admin can edit an article.
     *
     * @return void
     */
    public function testAdminsCanEditAnArticle()
    {
        $this->actingAs($this->admin);

        $draft = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'image' => UploadedFile::fake()->image('image.png'),
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $draft));
        $response->assertSuccessful();
        $response->assertSee('Edit');

        $response = $this->get(route('articles.edit', $draft));
        $response->assertSuccessful();

        $response = $this->put(route('articles.update', $draft), [
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'image' => UploadedFile::fake()->image('image.png'),
            'category_id' => Category::first()->id,
        ]);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();
    }

    /**
     * Testing that a visitor cannot edit an article.
     *
     * @return void
     */
    public function testVisitorsCantEditAnArticle()
    {
        $this->actingAs($this->visitor);

        $published = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'image' => UploadedFile::fake()->image('image.png'),
            'category_id' => Category::first()->id,
        ]);

        $response = $this->get(route('articles.show', $published));
        $response->assertSuccessful();
        $response->assertDontSee('Edit');

        $response = $this->get(route('articles.edit', $published));
        $response->assertForbidden();

        $response = $this->put(route('articles.update', $published), [
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'image' => UploadedFile::fake()->image('image.png'),
            'category_id' => Category::first()->id,
        ]);

        $response->assertForbidden();
    }
}
