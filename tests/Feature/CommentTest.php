<?php

namespace Tests\Feature;

use App\Category;
use Tests\TestCase;
use App\Enums\ArticleStatus;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CommentTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user with admin role.
     *
     * @var \App\User
     */
    protected $admin;

    /**
     * A user with no roles.
     *
     * @var \App\User
     */
    protected $visitor;

    /**
     * The setUp method.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->artisan('db:seed');
        $this->admin = factory(\App\User::class)->create()->assignRole('admin');
        $this->visitor = factory(\App\User::class)->create();
    }

    /**
     * Testing that an admin can comment on a published article.
     *
     * @return void
     */
    public function testAdminsCanCommentOnAPublishedArticle()
    {
        $this->actingAs($this->admin);

        $published = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->post(route('comments.store', $published), ['content' => 'Content']);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $draft = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->post(route('comments.store', $draft), ['content' => 'Content']);

        $response->assertForbidden();
    }

    /**
     * Testing that a visitor can comment on a published article.
     *
     * @return void
     */
    public function testVisitorCanCommentOnAPublishedArticle()
    {
        $this->actingAs($this->visitor);

        $published = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::PUBLISHED,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->post(route('comments.store', $published), ['content' => 'Content']);

        $response->assertRedirect();
        $response->assertSessionHasNoErrors();

        $draft = $this->admin->articles()->create([
            'title' => 'Title',
            'content' => 'Content',
            'status' => ArticleStatus::DRAFT,
            'category_id' => Category::first()->id,
        ]);

        $response = $this->post(route('comments.store', $draft), ['content' => 'Content']);

        $response->assertNotFound();
    }
}
