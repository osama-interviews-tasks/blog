# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-07-17
### Added
- Packages:
    - [spatie/laravel-permission](https://packagist.org/packages/spatie/laravel-permission)
    - [aldemeery/sieve](https://packagist.org/packages/aldemeery/sieve)
    - [aldemeery/enum-polyfill](https://packagist.org/packages/aldemeery/enum-polyfill)
    - [squizlabs/php_codesniffer](https://packagist.org/packages/squizlabs/php_codesniffer])
    - [laravelcollective/html](https://packagist.org/packages/laravelcollective/html)
- `.phpcs.xml` file defining the coding standards for the project.
- Article, Comment, Category, Roles and Permissions models and migrations.
- Database seeders for categories, admins, roles and permissions.
- The built-in default Laravel authentication.
- Listing all article for admins, and published articles for visitors.
- Filter classes to filter articles by title, conent and category.
- Creating new articles by admins.
- Updating articles by admins.
- Viewing an article.
- Add new comments on a published article.
- Feature tests for articles.
- Feature tests for comments.
- Add `.env.testing` file for test configuration.

### Changed
- The default string length to 191 instead of 255 to avoid possible errors with
  different database collations.
