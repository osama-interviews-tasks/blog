<?php

use App\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrCreate([
            'email' => 'admin@blog.com',
        ], [
            'name' => 'Administrator',
            'password' => bcrypt('password'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
        ]);

        $user->assignRole('admin');

        $this->command->getOutput()->writeln("<info>Created an admin, with email:</info> <comment>{$user['email']}</comment> <info>and password:</info> <comment>'password'</comment>");
    }
}
