<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        // Create permissions
        Permission::firstOrCreate(['name' => 'add articles']);
        Permission::firstOrCreate(['name' => 'edit articles']);

        // Create roles and assign created permissions
        Role::firstOrCreate(['name' => 'admin'])->givePermissionTo([
            'add articles',
            'edit articles'
        ]);
    }
}
