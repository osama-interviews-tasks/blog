<?php

namespace App;

use App\User;
use App\Comment;
use App\Category;
use App\Enums\ArticleStatus;
use App\Scopes\ArticleStatusScope;
use Illuminate\Database\Eloquent\Model;
use Aldemeery\Sieve\Concerns\Filterable;

class Article extends Model
{
    use Filterable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'title',
        'content',
        'image',
        'status',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ArticleStatusScope());
    }

    /**
     * List of individual filters to be used by the model.
     *
     * @return array
     */
    protected function filters()
    {
        return [
            'q' => \App\Http\Filters\SearchFilter::class,
            'category' => \App\Http\Filters\CategoryFilter::class,
        ];
    }

    /**
     * Image attribute accessor.
     *
     * @param string $path Image path stored in the database.
     *
     * @return string
     */
    public function getImageAttribute($path)
    {
        if ($path) {
            $path = asset(str_replace('public/', 'storage/', $path));
        } else {
            $path = 'https://via.placeholder.com/300X300?text=NO+IMAGE';
        }

        return $path;
    }

    /**
     * The user who created the article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * The article category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Comments on the article.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Is the article draft?
     *
     * @return boolean
     */
    public function isDraft()
    {
        return $this->status == ArticleStatus::DRAFT;
    }

    /**
     * Is the article published?
     *
     * @return boolean
     */
    public function isPublished()
    {
        return $this->status == ArticleStatus::PUBLISHED;
    }
}
