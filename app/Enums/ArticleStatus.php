<?php

namespace App\Enums;

use SplEnum;

class ArticleStatus extends SplEnum
{
    const DRAFT = 0;
    const PUBLISHED = 1;
}
