<?php

namespace App\Providers;

use App\Article;
use App\Observers\ArticleObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Setting up the default string length to 191 instead of 255
        // to avoid possible errors if a database collation other than
        // utf8mb4_unicode_ci is used.
        Schema::defaultStringLength(191);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Article::observe(ArticleObserver::class);
    }
}
