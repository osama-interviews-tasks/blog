<?php

namespace App\Scopes;

use App\Enums\ArticleStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class ArticleStatusScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder Builder instance.
     * @param \Illuminate\Database\Eloquent\Model   $model   Model beign queried.
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if (auth()->check() && !auth()->user()->hasRole('admin')) {
            $builder->where('status', ArticleStatus::PUBLISHED);
        }
    }
}
