<?php

namespace App\Observers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ArticleObserver
{
    /**
     * Request instance.
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Constructor.
     *
     * @param Request $request Request instance.
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the article "updating" event.
     *
     * @param \App\Article $article Article being updated.
     *
     * @return void
     */
    public function updating(Article $article)
    {
        if ($this->request->file('image') && $path = $article->getOriginal('image')) {
            Storage::delete($path);
        }
    }
}
