<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\Comments\CreateCommentRequest;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param CreateCommentRequest $request Request instance.
     * @param Article              $article Article being commented on.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCommentRequest $request, Article $article)
    {
        $inputs = $request->only('content');
        $inputs['user_id'] = auth()->id();

        $article->comments()->create($inputs);

        return redirect()->back();
    }
}
