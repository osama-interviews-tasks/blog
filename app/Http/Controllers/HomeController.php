<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * List all the articles.
     *
     * @param Request $request Incoming request instance.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $categories = Category::pluck('name', 'id')->toArray();

        $articles = Article::filter($request)
            ->withCount('comments')
            ->with('owner')
            ->latest()
            ->paginate();

        return view('home', compact('articles', 'categories'));
    }
}
