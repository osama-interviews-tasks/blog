<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\Articles\CreateArticleRequest;
use App\Http\Requests\Articles\UpdateArticleRequest;

class ArticleController extends Controller
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->middleware('permission:add articles')->only('create', 'store');
        $this->middleware('permission:edit articles')->only('edit', 'update');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id')->toArray();

        return view('articles.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateArticleRequest $request Incoming request instance.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticleRequest $request)
    {
        $inputs = $request->only('title', 'content', 'status', 'category_id');

        if ($request->file('image')) {
            $inputs['image'] = $request->file('image')->store('public');
        }

        auth()->user()->articles()->create($inputs);

        return redirect()->back()->with('status', 'Success!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Article $article Article to show.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        $article->load([
            'comments' => function ($query) {
                return $query->with('owner')->latest();
            },
        ]);

        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Article $article Article to edit.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        $categories = Category::pluck('name', 'id')->toArray();

        return view('articles.edit', compact('categories', 'article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateArticleRequest $request Incoming request instance.
     * @param \App\Article         $article Article to update.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticleRequest $request, Article $article)
    {
        $inputs = $request->only('title', 'content', 'status', 'category_id');

        if ($request->file('image')) {
            $inputs['image'] = $request->file('image')->store('public');
        }

        $article->update($inputs);

        return redirect()->back()->with('status', 'Success!');
    }
}
