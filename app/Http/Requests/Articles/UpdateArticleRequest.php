<?php

namespace App\Http\Requests\Articles;

use Illuminate\Foundation\Http\FormRequest;
use App\Enums\ArticleStatus;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $isAdmin = auth()->user()->hasRole('admin');

        return $isAdmin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:191',
            'content' => 'required|string',
            'image' => 'nullable|sometimes|image',
            'status' => 'required|in:' . implode(',', array_values(ArticleStatus::getConstList())),
            'category_id' => 'required|exists:categories,id',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $data = $this->all();
        $data['status'] = isset($data['draft']) ? ArticleStatus::DRAFT : ArticleStatus::PUBLISHED;

        $this->replace($data);
    }
}
