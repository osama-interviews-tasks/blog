<?php

namespace App\Http\Filters;

use Aldemeery\Sieve\Filter;
use Illuminate\Database\Eloquent\Builder;

class SearchFilter extends Filter
{
    /**
     * Filter values mappings.
     *
     * @var array
     */
    protected $mappings = [];

    /**
     * Filter records.
     *
     * @param Builder $builder Builder instance.
     * @param mixed   $value   Value passed in the query string.
     *
     * @return Builder
     */
    public function filter(Builder $builder, $value)
    {
        return $builder->where('title', 'LIKE', "%{$value}%")
            ->orWhere('content', 'LIKE', "%{$value}%");
    }
}
