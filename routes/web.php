<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('articles', 'ArticleController')->only('create', 'store', 'edit', 'update', 'show');
    Route::post('articles/{article}/comments', 'CommentController@store')->name('comments.store');
});
