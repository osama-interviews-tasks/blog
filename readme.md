# AqarMap Blog
A simple blogging system using Laravel framework and MySQL as DB storage.

* [Intro](#intro)
* [Installation](#installation)
* [Coding Standards](#coding-standards)
* [Testing](#testing)
* [Git Branching Model](#git-branching-model)
* [Packages](#packages)


## Intro

**Task Description:**
	- Only admin can add articles.
	- Admin should assign a category to each article they add.
	- Visitors can list the published articles.
	- Visitors can add comments on the published articles.
	- Visitors can filter articles by category.

**Roles:**
	*Admin:* Any registered user with `admin` role.
	*Visitor:* Any registered user with no roles.

 > NOTE:
 > For the lack of time, no great attention was paid to the front-end, as the main focus of the task was not the UI. The default Laravel styles were used along with bootstrap 4 to provide the UI using Blade template engine.
 
## Installation

 1. Open the command line and clone the project.
	``` bash
	$ git clone https://gitlab.com/osama-interviews-tasks/blog.git
	```
 2. `cd` into the cloned project.
	```bash
	$ cd blog
	```
 3. Install the dependencies.
	```bash
	$ composer install
	```
 4. Configure your development and testing databases in both `.env` and `.env.testing` files.

 5. Run the migrations and seed the database **(Required to add the roles and the permissions)**
	```bash
	$ php artisan migrate --seed
	```
 6. Start the server.
	```bash
	$ php artisan serve
	```
 7. Login as admin using the credentials  `email: admin@blog.com` and `password: password`

 8. Register and login as visitor.

### Coding Standards
`PSR1`, `PSR2`, `PSR12` and a combination of `PEAR`, `ZEND` and `Squiz` standards are used as coding standards for this project.
The rules can be found in the `.phpcs.xml` file in the project root directory.
To test the code against the mentioned standards, open the command line in the project root directory and run:

```bash
$ composer phpcs "./"
```

## Testing
The feature tests *for the required features* are written using PHPUnit Framework.

To run the tests, open the command line in the project root and run:
```bash
$ composer test
```
Or to run the tests and generate the coverage html report, run:
```bash
$ composer coverage
```
This will generate the coverage html in the `tests/report` directory.
> Note that the X-Debug extension is required to generate the html test coverage report.

### Git Branching Model
The [git-flow](https://nvie.com/posts/a-successful-git-branching-model/) branching model was followed during this project.
![image](https://nvie.com/img/git-model@2x.png)

You can check out the complete git history of the project to see how the project progressed.

## Packages
 - [aldemeery/sieve](https://packagist.org/packages/aldemeery/sieve) By Osama Aldemeery.
 - [aldemeery/enum-polyfill](https://packagist.org/packages/aldemeery/enum-polyfill) By Osama Aldemeery.
 - [spatie/laravel-permission](https://packagist.org/packages/spatie/laravel-permission) By Spatie.
 - [squizlabs/php_codesniffer](https://packagist.org/packages/squizlabs/php_codesniffer]) By Squiz Labs.
 - [laravelcollective/html](https://packagist.org/packages/laravelcollective/html) By Adam Engebretson and Matt Lantz.

